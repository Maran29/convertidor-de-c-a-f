package convertidor.marco.lucas.vega.pm.facci.convertidortarea1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Temperatura extends AppCompatActivity {

    Button Convertir= null;
    EditText edit_valor= null;
    TextView txtResult=null;
    Spinner spinner1=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperatura);

        Convertir =(Button)findViewById(R.id.btnCovertir);
        edit_valor=(EditText)findViewById(R.id.edit_valor1);
        txtResult=(TextView)findViewById(R.id.txtResult);
        spinner1=(Spinner)findViewById(R.id.spinner1);

        String []op={"Seleccionamos una opcion", "°C a °F", "°F a °C"};

        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,op);
        spinner1.setAdapter(adapter);
        Convertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edit_valor.getText().toString().equals("")) {
                    Toast mag = Toast.makeText(getApplicationContext(), "Escribe una cantidad", Toast.LENGTH_SHORT);
                    mag.show();
                } else {
                    Double c = Double.parseDouble(edit_valor.getText().toString());
                    Double res = null;
                    int select = spinner1.getSelectedItemPosition();


                    switch (select) {
                        case 0:
                            res=0.0;
                            Toast.makeText(getApplicationContext(),"Selecciona una opcion",Toast.LENGTH_SHORT).show();
                            break;

                        case 1:
                            res=1.8 *c +32;
                            break;

                        case 2:
                            res =(c - 32)/1.8;
                            break;


                    }
                    txtResult.setText(res.toString());
                }
            }
        });
    }

}
