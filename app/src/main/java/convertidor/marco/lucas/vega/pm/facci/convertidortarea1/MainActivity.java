package convertidor.marco.lucas.vega.pm.facci.convertidortarea1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnConver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnConver= (Button)findViewById(R.id.btnConver);

        btnConver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cambiar =new Intent(getApplicationContext(), Temperatura.class);
                startActivity(cambiar);
            }
        });
    }
}
